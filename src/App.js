import React from 'react';
import './App.css';

import Attendance from './Attendance';

function App() {
  return (
    <div className="App">
      <h2>Hello VAMK</h2>
      <Attendance title="Attendance code for Timo's course"/>
    </div>
  );
}

export default App;
